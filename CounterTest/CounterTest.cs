
namespace CounterTest;

[TestClass]
public class WordCounterTest
{
    public static string[] Lines = new string[1] {"auhausd asduahsdu, asdaaja, asdanasd: kioasidsa?"};
    public static Dictionary<string, int> Words = new Dictionary<string, int>();
    string phrase = Lines[0].Replace(@"[.,?:;!@#%&º\$\^\*(\)]", string.Empty);

    
    [TestMethod]
    public void ExtractWords_WithoutSymbolsInJustOneLineTest()
    {        
        if (phrase.Contains(@"[.,?:;!@#%&º\$\^\*(\)]"))
        {
            Assert.AreNotEqual(false, WordCounter.ExtractWords(WordCounter.Lines, WordCounter.Words));
        }
        else
        {
            Assert.AreEqual(true, WordCounter.ExtractWords(WordCounter.Lines, WordCounter.Words));
        }
    }

    [TestMethod]
    public void ExtractWords_WordLengthTest()
    {
        foreach(var word in phrase.Split(' ').Select(w => w.ToLower()))
        {
            if (word.Count() > 0)
            {
                Assert.AreEqual(true, WordCounter.ExtractWords(Lines, Words));
            }
            else
            {
                Assert.AreNotEqual(true, WordCounter.ExtractWords(Lines, Words));
            }
        }       
    }

    [TestMethod]
    public void ExtractWords_LineWithJustOneWordTest()
    {
        string[] Lines = new string[1] {"asudiasiasfasf"};
        Dictionary<string, int> Words = new Dictionary<string, int>();
        WordCounter.ExtractWords(Lines, Words);
        
        Assert.AreEqual(1, Words.Count());   
    }
    
    [TestMethod]
    public void ExtractWords_LineWithMoreThanOneWordTest()
    {
        string[] Lines = new string[1] {"asudiasiasfasf, ijfaisjjoasd, fkisodkfsmdf: asoijdaoa?"};
        Dictionary<string, int> Words = new Dictionary<string, int>();
        WordCounter.ExtractWords(Lines, Words);
        
        Assert.AreEqual(4, Words.Count());   
    }

    [TestMethod]
    public void ExtractWords_WordWithJustOneIncidenceTest()
    {
        string[] Lines = new string[1] {"asudiasiasfasf"};
        Dictionary<string, int> Words = new Dictionary<string, int>();
        WordCounter.ExtractWords(Lines, Words);
        
        foreach(var word in Words)
        {
            Assert.AreEqual(1, word.Value);   
        }
    }

    [TestMethod]
    public void ExtractWords_WordWithMoreThanOneIncidenceTest()
    {
        string[] Lines = new string[1] {"asudiajsl, asudiajsl, asudiajsl, asudiajsl: asudiajsl"};
        Dictionary<string, int> Words = new Dictionary<string, int>();
        WordCounter.ExtractWords(Lines, Words);
        
        foreach(var word in Words)
        {
            Assert.AreEqual(5, word.Value);   
        }
    }

    [TestMethod]
    public void ShowWords_WithJustOneWordTest()
    {
        string[] Lines = new string[1] {"asudiajsl, asudiajsl, asudiajsl, asudiajsl: asudiajsl"};
        Dictionary<string, int> Words = new Dictionary<string, int>();
        WordCounter.ExtractWords(Lines, Words);

        Dictionary<string, int> OrderedWords = 
        (from word in Words orderby word.Value descending select word)
        .Take(5).ToDictionary(word => word.Key, word => word.Value);
        
        foreach(var order in OrderedWords)
        {
            Assert.AreEqual("asudiajsl", order.Key);
        }
    }

    [TestMethod]
    public void ShowWords_WordWithMoreIncidenceTest()
    {
        string[] Lines = new string[1] {"aosdakmfas* asudiajsl% lpkfdokif? asudiajsl,: shushmsnf"};
        Dictionary<string, int> Words = new Dictionary<string, int>();
        WordCounter.ExtractWords(Lines, Words);

        Dictionary<string, int> OrderedWords = 
        (from word in Words orderby word.Value descending select word)
        .Take(1).ToDictionary(word => word.Key, word => word.Value);
        
        foreach(var order in OrderedWords)
        {            
            Assert.AreEqual("asudiajsl", order.Key);
        }
    }


}