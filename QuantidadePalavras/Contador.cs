//para cada linha do arquivo
    //para cada palavra da linha
        //se a palavra nao existe no dicionario
            //cria a palavra no dicionário com valor 1
        //senao
            //atualizar o contador com mais
        //fim
    //fim
//fim

//transforma o dicionario em uma lista
//ordena a lista de forma decrescente
//pega os 10 primeiro elementos da lista


using System.IO;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;

namespace Counter
{
    public class WordCounter
    {
        public static readonly string txtFile = @"C:\Users\luigi\ProjetoTECH\ContadorDePalavras\Shakespeare.txt";
        public static string[] Lines = File.ReadAllLines(txtFile);
        public static Dictionary<string, int> Words = new Dictionary<string, int>();    // DICTIONARY HAS A KEY N' A VALUE IN THE <> ------ <key, value> = <type, type>
        
        public static bool ExtractWords(string[] Lines, Dictionary<string, int> Words)
        {        
            //para cada linha do arquivo
            for (int i = 0; i < Lines.Length; i++)
            {
                //para cada palavra da linha                                            //options: spli, select, indexof, substring...
                string phrase = Regex.Replace(Lines[i], @"[.,?:;!@#%&º\$\^\*(\)]", string.Empty);//.Replace(' ', '');
                foreach(var word in phrase.Split(' ').Select(w => w.ToLower()))
                {
                    if (word.Count() > 0)
                    {
                        //se a palavra nao existe no dicionario
                        if(!Words.ContainsKey(word))
                        {
                            //cria a palavra no dicionário com valor 1
                            Words.Add(word, 1);
                        }
                        //senao
                        else
                        {
                            //atualizar o contador com mais
                            Words[word]++;
                        }
                    }
                }                   
            }
            return true;
        }

        public static bool ShowWords(Dictionary<string, int> Words)
        {
            Dictionary<string, int> OrderedWords =                      // LINQ METHOD
            (
                from word in Words
                orderby word.Value descending
                select word
            ).Take(10).ToDictionary(word => word.Key, word => word.Value);

            foreach(var order in OrderedWords)
            {
                System.Console.WriteLine(order);
                // System.Console.WriteLine($"{order.Key}, {order.Value}");
            }
            return true;
        }
    }
}


//MatchCollection matches = Regex.Matches(Words, Words[key]);